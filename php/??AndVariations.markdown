<?php
// -- Shorthand operators

//-------------------------
// Variable assignment
$null	= null;
$true	= true;
$false	= false;
$foo	= 'foo';
$bar	= 'bar';
$zero	= 0;
$zeroString = '0';
$empty 	= '';
// ------------------------

// ------------------------
// Ternary operator

// This
if ($true) {
    $result = 'foo';
} else {
    $result = 'bar';
}
// Is the same as this
$result = $true ? 'foo' : 'bar';

// Chaining Ternary operator

$false ? $false : ($true ? $true : $false) // true
$‌‌‌‌false ? $false : $true ? $true : $false // true
‌‌$false ? $false : ($true ? $false : $true) // false
‌‌$false ? $false : $true ? $false : $true // false


//---------------------------

//---------------------------
// Null coalescing operator ('??')

// This
$null		?? $foo; // 'foo'
$foo 		?? $bar; // 'foo'
$true 		?? $foo;; // 1
$false 		?? $foo;; // false
$empty 		?? $foo;; // ''
$zerostring ?? $foo;; // '0'
$zero 		?? $foo;; // 0

// Is the same as this

// Chaning null coalescing operator ('??')

$null ?? $null ?? $true // true
$null ?? $false ?? $true // false
$null ?? $foo ?? $true // foo

//---------------------------

//---------------------------
// Shorthand ternary operator ('?:')

// This
$null 		?: $foo; 	// foo
$foo 		?: $bar;  	// foo
$true 		?: $foo;  	// 1
$false 		?: $foo;	// foo
$empty 		?: $foo; 	// foo
$zerostring ?: $foo; 	// foo
$zero 		?: $foo; 	// foo

// So
$result = $true ?: $false;

// Is the same as
$result = $true ? $true : $false;

// And thus is equal to
if ($true) {
    $result = $true;
} else {
    $result = $false;
}

// Chaning shorthand ternary operator ('??')

$null ?: $null ?: $true // true
$null ?: $false ?: $true // true
$null ?: $foo ?: $true // foo
?>
